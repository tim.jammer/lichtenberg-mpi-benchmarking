#!/usr/bin/bash

for compiler in gcc/4.8.5 gcc/4.9.4 gcc/6.5.0 gcc/8.3.0 gcc/9.1.0 gcc/9.2.0 intel/2018.4 intel/2019.2 intel/2019.3 intel/2019.4 ; do
for mpi in openmpi/2.1.6 openmpi/3.1.3 openmpi/3.1.4 openmpi/3.1.5 openmpi/4.0.0 openmpi/4.0.1 openmpi/4.0.2 intelmpi/2018.4 intelmpi/2019.2 intelmpi/2019.3 intelmpi/2019.4 mpich/3.3.2 ; do

#Test combination
module purge

module -q load $compiler &> /dev/null
if [ $? -eq 0 ]
then
    module -q load $mpi &> /dev/null
    if [ $? -eq 0 ]
    then
        #it was possible to load this combination
        echo "$compiler,$mpi"
    fi
fi

module -q purge
done # end for mpi
done # end for compiler