import argparse
import pandas as pd
from termcolor import colored
from copy import deepcopy
import re


def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description='prints a summary of all measurments done')
    parser.add_argument("--experiement_path",
                        required=True,
                        help="Path where the Experiments should be stored")

    parser.add_argument("--combination_file", type=argparse.FileType('r'),
                        required=True,
                        help="File to read the combinations to benchmark")

    parser.add_argument("--custom_mpi", required=False, default=False,
                        action='store_true',
                        help="enable custom mpi implementations")
    # TODO add file to read settingf for custom implementations rather than hardcode them

    parser.add_argument("--output", type=argparse.FileType('w'), default="-",
                        required=False,
                        help="Output file")
    # default= stdout

    parser.add_argument("--single", required=False, default=False,
                        action='store_true',
                        help="only read single thresded benchmarks")

    parser.add_argument("--NO_color", required=False, default=False,
                        action='store_true',
                        help="not colorize output")

    parser.add_argument("--explanation", required=False, default=False,
                        action='store_true',
                        help="Show explanation of Metrics")

    parser.add_argument("--color_percent", type=int,
                        required=False, default="10",
                        help="set the percentage, in which to color")

    parser.add_argument("--small", type=int,
                        required=False, default="64",
                        help="message size to consider for a small msg size")
    parser.add_argument("--big", type=int,
                        required=False, default="1024",
                        help="message size to consider for a big msg size")

    args = parser.parse_args()
    return args


def read_file(fname, field):
    skip = 0
    numthreads = 1
    count_header = 0

    with open(fname, mode='r') as file:
        for line in file:
            skip += 1
            if "threads" in line:
                # extract only the number from string
                # #processes = 2 (threads: 16)
                numt = line.split(":")[1].split(")")[0]
                numthreads = numt.strip()
            if line.startswith("#--"):
                count_header += 1
                if count_header == 4:
                    break

    data = pd.read_table(fname, delimiter='\s\s+', skiprows=skip, engine='python')

    return data[field], int(numthreads)


def get_combination(ARGS, experiement_path, compiler, mpi):
    input_path = "%s/%s/%s" % (experiement_path, compiler, mpi)

    try:
        # bandwidth
        bw_intra, numt = read_file(input_path + "/" + "BW_INTRA.out", field='Mbytes/sec')
        assert numt == 1
        bw_inter, numt = read_file(input_path + "/" + "BW_INTER.out", field='Mbytes/sec')
        assert numt == 1
        if not ARGS.single:
            bw_inter_th, numt = read_file(input_path + "/" + "BW_INTER_TH.out", field='Mbytes/sec')
            # adjust axis, so that msg size is based on a per thread level
            bw_inter_th.index = [x / numt for x in bw_inter_th.index]

        # latency
        lat_intra, numt = read_file(input_path + "/" + "LAT_INTRA.out", field='t_avg[usec]')
        assert numt == 1
        lat_inter, numt = read_file(input_path + "/" + "LAT_INTER.out", field='t_avg[usec]')
        assert numt == 1
        if not ARGS.single:
            lat_inter_th, numt = read_file(input_path + "/" + "LAT_INTER_TH.out", field='t_avg[usec]')
            # adjust axis, so that msg size is based on a per thread level
            lat_inter_th.index = [x / numt for x in lat_inter_th.index]

        # allreduce
        all_intra, numt = read_file(input_path + "/" + "ALL_INTRA.out", field='t_avg[usec]')
        assert numt == 1
        all_inter, numt = read_file(input_path + "/" + "ALL_INTER.out", field='t_avg[usec]')
        assert numt == 1
        if not ARGS.single:
            all_inter_th, numt = read_file(input_path + "/" + "ALL_INTER_TH.out", field='t_avg[usec]')
            # adjust axis, so that msg size is based on a per thread level
            all_inter_th.index = [x / numt for x in all_inter_th.index]

        if not ARGS.single:
            data = pd.DataFrame({'BW_INTRA': bw_intra, 'BW_INTER': bw_inter, 'BW_INTER_TH': bw_inter_th,
                                 'LAT_INTRA': lat_intra, 'LAT_INTER': lat_inter, 'LAT_INTER_TH': lat_inter_th,
                                 'ALL_INTRA': all_intra, 'ALL_INTER': all_inter, 'ALL_INTER_TH': all_inter_th
                                 })
        else:
            data = pd.DataFrame({'BW_INTRA': bw_intra, 'BW_INTER': bw_inter,
                                 'LAT_INTRA': lat_intra, 'LAT_INTER': lat_inter,
                                 'ALL_INTRA': all_intra, 'ALL_INTER': all_inter
                                 })

        return data

    except (pd.errors.EmptyDataError,FileNotFoundError) as e:
        # skip if data was not found
        return None



def read_all_data(ARGS, experiement_path, combinations):
    data = dict()

    for c in combinations:
        compiler, mpi = c.split(',', 1)
        combination_data = get_combination(ARGS, experiement_path, compiler.rstrip(), mpi.rstrip())
        if combination_data is not None:
            data[c] = combination_data

    return data


def filter_relevant_data(ARGS, data):
    new_data = []

    for key in data:
        dat = data[key]
        big = dat.loc[[ARGS.big]]
        small = dat.loc[[ARGS.small]]

        # TODO customize this?
        # SMALL: BW,lat,all, BIG: BW,lat,all

        if ARGS.single:
            row = [key.rstrip(), big['BW_INTER'].values[0], big['LAT_INTER'].values[0], big['ALL_INTER'].values[0],
                   small['BW_INTER'].values[0], small['LAT_INTER'].values[0], small['ALL_INTER'].values[0]
                   ]
        else:
            row = [key.rstrip(), big['BW_INTER'].values[0], big['BW_INTER_TH'].values[0], big['LAT_INTER'].values[0],
                   big['LAT_INTER_TH'].values[0], big['ALL_INTER'].values[0], big['ALL_INTER_TH'].values[0],
                   small['BW_INTER'].values[0], small['BW_INTER_TH'].values[0], small['LAT_INTER'].values[0],
                   small['LAT_INTER_TH'].values[0], small['ALL_INTER'].values[0], small['ALL_INTER_TH'].values[0]]

        new_data.append(row)
    # print(row)

    return new_data


def show_explanation(ARGS):
    print("BW: Bandwidth (in MB/s) for a msg-size of %iB" % (ARGS.big))
    if not ARGS.single:
        print("BW_TH: Bandwidth (in MB/s) for a msg-size of %iB in the multi-threaded case" % (ARGS.big))
    print("LAT: Latency (in us) for a msg-size of %iB" % (ARGS.big))
    if not ARGS.single:
        print("LAT_TH: Latency (in us) for a msg-size of %iB in the multi-threaded case" % (ARGS.big))
    print("ALL: duration of an allreduce operation (in us) for a msg-size of %iB" % (ARGS.big))
    if not ARGS.single:
        print("ALL_TH: duration of an allreduce operation (in us) for a msg-size of %iB in the multi-threaded case" % (
            ARGS.big))
    print("")
    print("bw: Bandwidth (in MB/s) for a msg-size of %iB" % (ARGS.small))
    if not ARGS.single:
        print("bw_th: Bandwidth (in MB/s) for a msg-size of %iB in the multi-threaded case" % (ARGS.small))
    print("lat: Latency (in us) for a msg-size of %iB" % (ARGS.small))
    if not ARGS.single:
        print("lat_th: Latency (in us) for a msg-size of %iB in the multi-threaded case" % (ARGS.small))
    print("all: duration of an allreduce operation (in us) for a msg-size of %iB" % (ARGS.small))
    if not ARGS.single:
        print("all_th: duration of an allreduce operation (in us) for a msg-size of %iB in the multi-threaded case" % (
            ARGS.small))


# , "LAT", "ALL", "bw", "lat", "all"]


def print_data(ARGS, data):
    num_metrics = 6 * 2
    lower_is_better = [False, False, False, True, True, True, True, False, False, True, True, True, True]
    header = ["BW", "BW_TH", "LAT", "LAT_TH", "ALL", "ALL_TH", "bw", "bw_th", "lat", "lat_th", "all", "all_th"]
    if ARGS.single:
        num_metrics = 3 * 2
        lower_is_better = [False, False, True, True, False, True, True]
        header = ["BW", "LAT", "ALL", "bw", "lat", "all"]

    # find best and worst implementation for coloring
    if ARGS.color_percent <= 0:
        ARGS.color_percent = 1
    min = deepcopy(data[0])
    max = deepcopy(data[0])
    for row in data:
        for i in range(1, num_metrics + 1):
            if row[i] > max[i]:
                max[i] = row[i]
            if row[i] < min[i]:
                min[i] = row[i]

    min_limit = [float(i) * (100.0 + ARGS.color_percent) / 100 for i in min[1:num_metrics + 1]]
    max_limit = [float(i) * (100.0 - ARGS.color_percent) / 100 for i in max[1:num_metrics + 1]]
    min_limit = ["MPI"] + min_limit
    max_limit = ["MPI"] + max_limit

    # print header
    print("")
    print('%-30s' % ("MPI Implementation"), end='')
    for s in header:
        print('%10s' % (s), end='')
    print("")

    # print data
    for row in data:
        print('%-30s' % (row[0]), end='')
        for i in range(1, num_metrics + 1):
            value = row[i]
            if not ARGS.NO_color \
                    and (lower_is_better[i] and value < min_limit[i]) \
                    or (not lower_is_better[i] and value > max_limit[i]):
                print(colored('%10.2f' % (value), 'green'), end='')
            else:
                if not ARGS.NO_color \
                        and (not lower_is_better[i] and value < min_limit[i]) \
                        or (lower_is_better[i] and value > max_limit[i]):
                    print(colored('%10.2f' % (value), 'red'), end='')
                else:
                    print('%10.2f' % (row[i]), end='')
        # print(colored('%10.2f' % (row[2]),'red'),end = '')
        print("")


def main():
    ARGS = parse_command_line_args()

    combinations = [line for line in ARGS.combination_file]
    if ARGS.custom_mpi:
        combinations.append("gcc/8.3.0,mvapich/2.3.3")

    data = read_all_data(ARGS, ARGS.experiement_path, combinations)

    data = filter_relevant_data(ARGS, data=data)
    # for i in range(10):
    #    print('%-12i%-12i' % (10 ** i, 20 ** i))
    if ARGS.explanation:
        show_explanation(ARGS)
    print_data(ARGS, data)

    #ARGS.output.write("done")


if __name__ == "__main__":
    main()
