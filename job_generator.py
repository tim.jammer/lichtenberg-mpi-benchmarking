import argparse
import os
import shutil


def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description='Generates A jobscript for benchmarking of several mpi implementations and initialized the needed directory hierarchy')
    parser.add_argument("--experiement_path",
                        required=True,
                        help="Path where the Experiments should be stored")

    parser.add_argument("--combination_file", type=argparse.FileType('r'),
                        required=True,
                        help="File to read the combinations to benchmark")

    parser.add_argument("--output", type=argparse.FileType('w'), default="job_script.sh",
                        required=False,
                        help="Output file")
    parser.add_argument("--NO-MT", required=False, default=False,
                        action='store_true',
                        help="Do not test Multi Threaded benchmark as well")

    parser.add_argument("--job_name",
                        required=False, default="MPI_Benchmark",
                        help="name of Job")
    parser.add_argument("--job_out",
                        required=False, default="benchmark_job.log",
                        help="Name of Jobs Output file")

    parser.add_argument("--imb",
                        required=False, default="/home/tj75qeje/lichtenberg-mpi-benchmarking/mpi-benchmarks",
                        help="path to imb benchmark src")

    parser.add_argument("--message_length", required=False,
                        default="2,4,8,16,32,64,128,256,512,1024",
                        help="comma separated list of message lengths to use")

    parser.add_argument("--phase1", required=False, default=False,
                        action='store_true',
                        help="run on phase 1")

    parser.add_argument("--custom_mpi", required=False, default=False,
                        action='store_true',
                        help="enable custom mpi implementations")
    #TODO add file to read settingf for custom implementations rather than hardcode them

    parser.add_argument("--NO_BW", required=False, default=False,
                        action='store_true',
                        help="Do not test bandwidth")
    parser.add_argument("--NO_LAT", required=False, default=False,
                        action='store_true',
                        help="Do not test latency")
    parser.add_argument("--NO_ALL", required=False, default=False,
                        action='store_true',
                        help="Do not test allreduce")
    parser.add_argument("--single", required=False, default=False,
                        action='store_true',
                        help="only run single thresded benchmarks")

    parser.add_argument("--time_per_benchmark", required=False, type=int, default=2, action='store',
                        help="time per benchmark in minutes to set slurm job time")
    parser.add_argument("--max_time", required=False, default=1440, type=int, action='store',
                        help="maximum time allowed for slurm job in minutes")
    # default= 1 day

    args = parser.parse_args()
    return args


def get_custom_combination(ARGS, compiler, mpi_name, mpi_path):
    output_path = "%s/%s/%s" % (ARGS.experiement_path, compiler, mpi_name)
    mpi_benchmark_folder = output_path + "/mpi_benchmark"
    # create this directory
    os.makedirs(mpi_benchmark_folder, exist_ok=True)

    calls = ""
    num_benchmarks = 0
    if not ARGS.NO_BW:
        num_benchmarks += 1
        calls = calls + get_bw_call(ARGS, output_path)
    if not ARGS.NO_LAT:
        num_benchmarks += 1
        calls = calls + get_lat_call(ARGS, output_path)
        num_benchmarks += 1
    if not ARGS.NO_ALL:
        calls = calls + get_all_call(ARGS, output_path)

    modules = """
module purge
module load %s
""" % (compiler)

    # cp imb src
    # sent env vars
    # make imb
    initialization = """
MPI_ROOT_PATH=%s
OLD_PATH=$PATH
OLD_CPATH=$CPATH
OLD_LIBRARY_PATH=$LIBRARY_PATH
OLD_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
export PATH=$PATH:${MPI_ROOT_PATH}/bin
export CPATH=$CPATH:${MPI_ROOT_PATH}/include
export LIBRARY_PATH=$LIBRARY_PATH:${MPI_ROOT_PATH}/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${MPI_ROOT_PATH}/lib
# make imb
OLD_WD=$(pwd)
cp -r %s %s
cd %s
CC=mpicc CXX=mpicxx make
PATH=$PATH:$(pwd)      
""" % (mpi_path, ARGS.imb,mpi_benchmark_folder,mpi_benchmark_folder)

    ## run

    # remove imb
    # unset env vars
    wrap_up = """
# remove src
cd ..
rm -rf mpi_benchmark
cd $OLD_WD
# old env vars    
export PATH=$OLD_PATH
export CPATH=$OLD_CPATH
export LIBRARY_PATH=OLD_LIBRARY_PATH
export LD_LIBRARY_PATH=OLD_LD_LIBRARY_PATH
"""

    return "%s%s%s%s" % (modules, initialization, calls, wrap_up), num_benchmarks


def get_combination(ARGS, compiler, mpi):
    output_path = "%s/%s/%s" % (ARGS.experiement_path, compiler, mpi)

    # create this directory
    os.makedirs(output_path, exist_ok=True)

    calls = ""
    num_benchmarks = 0
    if not ARGS.NO_BW:
        num_benchmarks += 1
        calls = calls + get_bw_call(ARGS, output_path)
    if not ARGS.NO_LAT:
        num_benchmarks += 1
        calls = calls + get_lat_call(ARGS, output_path)
        num_benchmarks += 1
    if not ARGS.NO_ALL:
        calls = calls + get_all_call(ARGS, output_path)

    modules = """
module -q purge
module -q load %s %s imb
START_TIME=$(date +%%s)
""" % (compiler, mpi)

    wrapup="""
END_TIME=$(date +%%s)
echo "Benchmark for %s,%s took $((END_TIME - START_TIME)) seconds"
""" % (compiler, mpi)

    return "%s%s%s" % (modules, calls,wrapup), num_benchmarks


multi_threaded_call = "OMP_PROC_BIND=\"close\" OMP_PLACES=\"threads\" OMP_NUM_THREADS=$per_socket srun --cpus-per-task $per_socket -n $mt_processes --cpu-bind=verbose,ldoms IMB-MT -thread_level multiple -count $MESSAGE_LENGTHS %s"


def get_lat_call(ARGS, output_path):
    call_single_intra = "OMP_NUM_THREADS=1 mpirun -n $ONE_NODE_NTASKS IMB-MT -thread_level single -count $MESSAGE_LENGTHS PingPongMT >> %s/LAT_INTRA.out" % (
        output_path)
    call_single_inter = "OMP_NUM_THREADS=1 mpirun -n $SLURM_NTASKS IMB-MT -thread_level single -count $MESSAGE_LENGTHS PingPongMT >> %s/LAT_INTER.out" % (
        output_path)

    call_multi_inter = multi_threaded_call % ("PingPongMT >> " + output_path + "/LAT_INTER_TH.out")

    if ARGS.single: # if disabled
        call_multi_inter=""

    return "%s\n%s\n%s\n" % (call_single_intra, call_single_inter, call_multi_inter)


def get_all_call(ARGS, output_path):
    call_single_intra = "OMP_NUM_THREADS=1 mpirun -n $ONE_NODE_NTASKS IMB-MT -thread_level single -count $MESSAGE_LENGTHS AllReduceMT >> %s/ALL_INTRA.out" % (
        output_path)
    call_single_inter = "OMP_NUM_THREADS=1 mpirun -n $SLURM_NTASKS IMB-MT -thread_level single -count $MESSAGE_LENGTHS AllReduceMT >> %s/ALL_INTER.out" % (
        output_path)

    call_multi_inter = multi_threaded_call % ("AllReduceMT >> " + output_path + "/ALL_INTER_TH.out")
    if ARGS.single: # if disabled
        call_multi_inter=""

    return "%s\n%s\n%s\n" % (call_single_intra, call_single_inter, call_multi_inter)


def get_bw_call(ARGS, output_path):
    call_single_intra = "OMP_NUM_THREADS=1 mpirun -n $ONE_NODE_NTASKS IMB-MT -thread_level single -count $MESSAGE_LENGTHS BibandMT >> %s/BW_INTRA.out" % (
        output_path)
    call_single_inter = "OMP_NUM_THREADS=1 mpirun -n $SLURM_NTASKS IMB-MT -thread_level single -count $MESSAGE_LENGTHS BibandMT >> %s/BW_INTER.out" % (
        output_path)

    call_multi_inter = multi_threaded_call % ("BibandMT >> " + output_path + "/BW_INTER_TH.out")
    if ARGS.single: # if disabled
        call_multi_inter=""

    return "%s\n%s\n%s\n" % (call_single_intra, call_single_inter, call_multi_inter)


# define some env vars to use in the job script
def get_env_vars(ARGS):
    env = """
ONE_NODE_NTASKS=$(( SLURM_NTASKS / SLURM_JOB_NUM_NODES ))
MESSAGE_LENGTHS="%s"
per_socket=6
mt_processes=$((SLURM_JOB_NUM_NODES * 4))
"""
    if ARGS.phase1:
        env = """
        ONE_NODE_NTASKS=$(( SLURM_NTASKS / SLURM_JOB_NUM_NODES ))
        MESSAGE_LENGTHS="%s"
        per_socket=8
        mt_processes=$((SLURM_JOB_NUM_NODES * 2))
        """
    return env % (ARGS.message_length)


def get_slurm_header(ARGS, num_bench):
    header = """#!/bin/bash
#SBATCH --nodes 2
#SBATCH --mem-per-cpu=1600

#SBATCH --ntasks 48
#SBATCH -C avx2&mpi

#SBATCH --time %i
#SBATCH --output %s/%s
#SBATCH --job-name %s
"""

    if ARGS.phase1:
        header = """#!/bin/bash

#SBATCH --nodes 2
#SBATCH --mem-per-cpu=1600

#SBATCH --ntasks 32
#SBATCH -C avx&mpi

#SBATCH --time %i
#SBATCH --output %s/%s
#SBATCH --job-name %s
"""

    time = num_bench * ARGS.time_per_benchmark
    if time > ARGS.max_time:
        time = ARGS.max_time
    return header % (time, ARGS.experiement_path, ARGS.job_out, ARGS.job_name)


def build_script_body(ARGS):
    num_benchmarks = 0
    body = ""

    for line in ARGS.combination_file:
        compiler, mpi = line.split(',', 1)
        this_combination, executed_bench = get_combination(ARGS, compiler.rstrip(), mpi.rstrip())
        body = body + this_combination
        num_benchmarks = num_benchmarks + executed_bench

    # custom combination
    if ARGS.custom_mpi:
        this_combination, executed_bench = get_custom_combination(ARGS, "gcc/8.3.0", "mvapich/2.3.3",
                                                              "/work/scratch/tj75qeje/mvapich_install")
        body = body + this_combination
        num_benchmarks = num_benchmarks + executed_bench

    return body, num_benchmarks


def main():
    ARGS = parse_command_line_args()

    body, num_benchmarks = build_script_body(ARGS)

    header = get_slurm_header(ARGS, num_benchmarks)
    env = get_env_vars(ARGS)

    ARGS.output.write(header)
    ARGS.output.write(env)
    ARGS.output.write(body)

    print("done")


if __name__ == "__main__":
    main()
